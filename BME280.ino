#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>

#include <ESP8266mDNS.h>

//file with credentials to your wifi. File added to .gitignore
#include "credentials.h"

#include <BME280I2C.h>
#include <Wire.h>

BME280I2C bme;                   // Default : forced mode, standby time = 1000 ms
                                 // Oversampling = pressure ×1, temperature ×1, humidity ×1, filter off,
#define SDA 2
#define SCL 3
bool metric = true;


#if !defined(WLAN_SSID)
#define WLAN_SSID ""
#endif
#if !defined(WLAN_PASS)
#define WLAN_PASS ""
#endif
ESP8266WebServer server(80);
String webString="";

//void printValues();

void setup(void)
{

  pinMode(SDA, OUTPUT);
  pinMode(SCL, OUTPUT);
  Wire.begin(SDA,SCL);

  bool status = bme.begin();
  if (!status) {
    webString = "Could not find a valid BME280 sensor, check wiring!";
  }

  WiFi.begin(WLAN_SSID, WLAN_PASS);

  while (WiFi.status() != WL_CONNECTED) 
  {
    delay(500);
  }

  if (!MDNS.begin("espMPokoj")) 
  {
    webString = "Error setting up MDNS responder!";
    while(1) 
    { 
      delay(1000);
    }
  }

  server.on("/", []()
  {
    String webString = printValues();
    server.send(200, "text/plain", webString);
    delay(100);
  });

  server.on("/json", []()
  {
    String webString = printJson();
    server.send(200, "application/json", webString);
    delay(100);
  });

  server.begin();
  MDNS.addService("http", "tcp", 80);
}

void loop(void)
{
  server.handleClient();
} 

String printValues(void) {

  float temp(NAN), hum(NAN), pres(NAN);
   uint8_t pressureUnit(1);                                           // unit: B000 = Pa, B001 = hPa, B010 = Hg, B011 = atm, B100 = bar, B101 = torr, B110 = N/m^2, B111 = psi
   bme.read(pres, temp, hum, metric, pressureUnit);                   // Parameters: (float& pressure, float& temp, float& humidity, bool celsius = false, uint8_t pressureUnit = 0x0)

   String output = "";
   output += "Temperature = ";
   output += temp;
   output += "°"+ String(metric ? 'C' :'F')+"\n";

   output += "Pressure = ";

   output += pres;
   output += " hPa\n";

   output += "Humidity = ";
   output += hum;
   output += " %\n";

   return output;
 }

 String printJson(void) {

  float temp(NAN), hum(NAN), pres(NAN);
   uint8_t pressureUnit(1);                                           // unit: B000 = Pa, B001 = hPa, B010 = Hg, B011 = atm, B100 = bar, B101 = torr, B110 = N/m^2, B111 = psi
   bme.read(pres, temp, hum, metric, pressureUnit);                   // Parameters: (float& pressure, float& temp, float& humidity, bool celsius = false, uint8_t pressureUnit = 0x0)

   String output = "{";
   output += "\"sensor\" : \"BME280\",\n";
   output += "\"name\" : \"m_pokoj\",\n";
   output += "\"temperature\" : \"" + String(temp) + "\",\n";
   output += "\"temperature_unit\" : \"°"+ String(metric ? 'C' :'F')+"\",\n";
   output += "\"pressure\" : \"" + String(pres) + "\",\n";
   output += "\"pressure_unit\" : \"hPa\",\n";
   output += "\"humidity\" : \"" + String(hum) + "\",\n";
   output += "\"humidity_unit\" : \"%\"\n";
   output += "}";

   return output;
 }